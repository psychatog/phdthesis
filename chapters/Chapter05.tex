% Chapter 5: Methodology for design crease patterns for the proposed robot

The methodology applied to create crease patterns with the proposed robot is based on shapes of surface of revolution (or rotational sweep).
In \cite{Mitani2009}, a software called ORI-REVO, able to create 3D paper structures from a 2D poly-line, is described. 
This program can be used to create a vast number of 3D shapes that can be used in different types of applications. The methodology used in ORI-REVO is explained in \cite{Mitani2009}. ORI-REVO can be used to create crease patterns less complex than other very known software such as: Tree Maker or Origamizer, but is limited to figures using only surface of revolution. Although the crease pattern is not very complicated to be folded by hand, proper adaptations of this methodology must be carried out in order to be implemented into the proposed robot. To understand better the adaptations executed to this method let’s observe one crease pattern created using ORI-REVO in Fig. \ref{Fig:ORIRevo}.

\begin{figure}[h]
\centering
    \includegraphics[width=0.9\linewidth]{Chap05_ORIRevo.png} 

    \caption{Example of an origami hat crease pattern developed with ORI-REVO. Blue lines: valley folds, red lines: mountain folds, circles: folds difficult to be executed using robots.}  
    \label{Fig:ORIRevo}
\end{figure} 

The origami pattern developed with ORI-REVO or any of the previously mentioned software (e.g. Tree-Maker, or Origamizer), are intended to be assembled by hand, and have folds that are very difficult to execute with a robot due to handling problems. For example, the folds highlighted in orange circles in Fig. \ref{Fig:ORIRevo} are folding lines allocated inside the paper, and do not overstep from one edge to the other in the crease pattern. These types of lines require multiple manipulations during folding process, making them not suitable for automation. In this chapter, a modified version of the SOR in \cite{Mitani2009} is proposed. This new methodology, uses a combination of simple folding patterns with gluing areas.
In our previous works, there are times where we refer to this mixture between folding and gluing as ``Norigami''. The word Norigami comes from the mixture between three words in Japanese language: ``\textit{nori}'' that means glue, ``\textit{ori}'' that means fold, and ``\textit{kami/gami}'' that means paper. As can be deduced from its name, in norigami, \textbf{\textit{simple origami}} crease patterns are used in combination with  \textbf{\textit{small glued segments}}, to create 3D shape figures using paper or similar flexible materials. The main idea in norigami is to reduce the complexity in the folding process by reducing the number of manipulations required to build a 3D-paper shape. This is possible thanks to combining manipulations within the folding process and fusing some crease lines in nodes of the crease pattern, changing the kinematics in the final 3D shape.


\section{Adaptations of the surface of revolution methodology for automation}
In this chapter, a modified version of the method in \cite{Mitani2009} is applied to create patterns able to be folded by the proposed robot~\cite{Romero2016}. Having a profile as in Fig.\ref{Fig:SOR01}a, the resulting model consists of a set of trapezoids created from rotating the profile $2\pi/N rad$ with resulting flaps that represent the areas where the glue is applied (see Fig\ref{Fig:SOR01}b).

\begin{figure}[h]
\centering
    \begin{tabular}{cc}
    \includegraphics[width=.5\linewidth]{Chap05_SOR01a.png} &
    \includegraphics[width=.5\linewidth]{Chap05_SOR01b.png} \\
    (a)&(b)
    \end{tabular}
    \caption{An example of a shape with two stages, (a) profile, (b) solid 3D structure after rotational sweep, $N=6$}  
    \label{Fig:SOR01}
\end{figure} 

In the cases where there is only one stage in the profile (see Fig.\ref{Fig:SOR02}a and \ref{Fig:SOR02}b), the flaps can rotate freely because there is no connection within another flap. However, adding new stages like in Fig.\ref{Fig:SOR02}c and \ref{Fig:SOR02}d creates new connections within flaps. This problem can be solved if the angle of rotation of the flaps $\gamma$ is equal to zero. On the other hand, this only works if the flaps are placed outside the paper’s shape as in Fig.\ref{Fig:SOR02}d, and it is not possible to place the flaps inside the structure. We propose as solution to this problem to perform cut lines in specified sectors of the crease pattern. The proper location of these cut lines are explained later in this chapter.

\begin{figure}[h]
\centering
    \begin{tabular}{cc}
    \includegraphics[width=.65\linewidth]{Chap05_SOR02a.png} & \includegraphics[width=.25\linewidth]{Chap05_SOR02b.png} \\
    (a)&(b) \\
    \includegraphics[width=.65\linewidth]{Chap05_SOR02c.png} & \includegraphics[width=.25\linewidth]{Chap05_SOR02d.png} \\
    (c)&(d)
    \end{tabular}
    \caption{An example of a crease pattern, (a) crease pattern one stage, (b) folded shape with flaps of one stage, (c) crease pattern of multiple stages, (d) folded shape with flaps of multiple stages}  
    \label{Fig:SOR02}
\end{figure} 

\begin{figure}[h]
\centering
    \begin{tabular}{cc}
    \includegraphics[width=.45\linewidth]{Chap05_SurfRev_Proc_A.png} &
    \includegraphics[width=.45\linewidth]{Chap05_SurfRev_Proc_B.png} \\
    (a)&(b)
    \end{tabular}
    \caption{Input profile and crease points defining a sequence of segments of $K$ vertices $(n=0\cdots K)$. (a) input profile, (b) segment of the crease pattern. The dashed gray line is a middle valley fold. The gray area is the gluing area and the white area is the area in the surface of the 3D object (non-glued area)}  
    \label{Fig:SR}
\end{figure} 

In Fig. \ref{Fig:SR}a, $P_{(n,x)}$, and $P_{(n,y)}$ denote the $x$ and $y$ coordinates respectively of the vertex $P_n$ in the input profile. For each point $P_n$ in the input profile, two points ($P1'_n$  and $P2'_n$) are created in the crease pattern. In Fig. \ref{Fig:SR}b, $P'_{(n,x)}$, and $P'_{(n,y)}$ denote the $x$, and $y$ coordinates respectively of the vertex $P'_n$ in the crease pattern. The new points $P1'_n$  and $P2'_n$ have the same $y$ coordinate but different $x$ coordinates. Using the information in Fig.\ref{Fig:SR}a, the values of  Fig.\ref{Fig:SR}b can be calculated as follows:

\begin{equation}
\begin{array}{lr}
	w_n=2P_{(n,x)}\sin⁡{\frac{\pi}{N}}, & W=max(w_n).
	\label{Eq:SOR01}
	\end{array}
\end{equation}


\begin{equation}
	b_n=\frac{W - w_n}{2}.
	\label{Eq:SOR02} 
\end{equation}

\begin{equation}
	l_n= \vert\vert P_n-P_{(n-1)} \vert\vert.
	\label{Eq:SOR03} 
\end{equation}

\begin{equation}
	\begin{array}{lr}
		P1'_{(n,x)}=\frac{W-w_n}{2}, & P2'_{(n,x)}=\frac{W+w_n}{2}.
		\label{Eq:SOR04} 
	\end{array}
\end{equation}

\begin{equation}
 	P1'_{(n,y)}=P2'_{(n,y)}=P'_{(n,y)}= \left\{ 
	\begin{array}{lr}
        0,	& if (n=0),\\
        P'_{(n-1,y)} + \sqrt{ l_{n}^{2}- \left( \frac{w_n-w_{n-1}}{2} \right) ^{2} }, 	 & if (n>0).
	\end{array}
	\right .
	\label{Eq:SOR05}	
\end{equation}
where $w_n$ and $b_n$ represent the width of the non-glued and glued areas respectively of each segment at point $P_n$ of the profile, and $W$ represents the size of each segment in the crease pattern. Note that the $N$ segments forming the crease pattern are separated by mountain folds placed at distances multiples of $W$. The robot folds the paper sheet after the crease pattern is designed. The desired width between folds ($W/2$) and the shape of the gluing stamp are extracted from the designed pattern. Above information is stored in the NXT bricks (See Fig.\ref{Fig:Folding-Gluing} parts F5 and G7) together with the program for the folding-gluing procedure.

After the crease pattern is concluded, the next step is to generate the 3D model using the information previously calculated. Apart of the values calculated using equations (\ref{Eq:SOR01}) - (\ref{Eq:SOR05}), the angle between each one of the segments $l_n$, denoted in this paper as $\theta_n$ can be calculated using the following equations:

\begin{equation}
	\theta_n=\tan^{-1} \left( \frac{U_{(n,x)} V_{(n,z)}-U_{(n,z)} V_{(n,x)}}{U_{(n,x)} V_{(n,x)}+U_{(n,z)} V_{(n,z)}} \right)
	\label{Eq:SOR06} 
\end{equation}

\begin{equation}
 	U_n= \left\{ 
	\begin{array}{lr}
         \mbox{[}\ P_{(n,x)} \mbox{~~~}\ 0 \mbox{]}\ ,	 &if (n=0), \\
         \mbox{[}\ P_{(n,x)}-P_{(n-1,x)} \mbox{~~~}\ P_{(n,z)}-P_{(n-1,z)} \mbox{]}\ ,	 &if (n>0).
	\end{array}
	\right .
	\label{Eq:SOR07}	
\end{equation}

\begin{equation}
 	V_n = \left [ P_{(n+1,x)}-P_{(n,x)} \mbox{~~~}\ P_{(n+1,z)}-P_{(n,z)} ] \right .
	\label{Eq:SOR08}	
\end{equation}

The value of $\theta_n$ can be used to define the position of the flap. Using all the information obtained from equations (\ref{Eq:SOR01}) - (\ref{Eq:SOR08}) the values can be applied into geometrical transformation matrices and obtain the location of all points in the 3D model and their flaps. One of the main differences between our proposed method and \cite{Mitani2009} is that our crease pattern could have cutting lines (see the red dashed lines in  Fig. \ref{Fig:SOR02}c). The value of $\theta_n (\theta_n>\pi)$ also allows the condition for adding cutting lines for interior flaps. If $\theta_n<\pi$ the paper can be flat-folded but it is complicated for the robot, so we decide to cut all flaps. These new lines make it possible to rotate the flaps an angle $\gamma$ into any value, except multiples of $\pi$. The value of $\gamma$ can be used for planning/simulation of the folding motion in self-folding robots like in \cite{Felton2014}. This new freedom movement in the flaps allows to avoid some problems that in \cite{Mitani2009} were impossible to solve; e.g. the flaps can also be allocated inside the structure. Although there are some angles where the flaps crash within the structure or other flaps, performing these cut lines allows the user to have more freedom in the creation of 3D shapes.

\begin{figure}[h]
\centering
    \includegraphics[width=.95\linewidth]{Chap05_SOR04.png}
    \caption{Norigami hat crease pattern and 3D shapes made by the regular surface of revolution.}  
    \label{Fig:SOR04}
\end{figure} 

%------------------------------------------------------------------------------
% Independent gluing segments methodology
%------------------------------------------------------------------------------
\subsection{Patterns with independent gluing segments}
\label{Sec:TIGS}
As mentioned before, even performing cuts into the crease pattern, there are some cases when the flaps crash between the main structure or another flap. To create the norigami pattern that never has flap penetration, we propose to assume each line segment in the profile's poly-line as an independent line as shown in Fig. \ref{Fig:SOR06}. The resulting crease patterns decrease the gluing area in comparison with the ones in Fig. \ref{Fig:SOR04}.

\begin{figure}[h]
\centering
    \includegraphics[width=.95\linewidth]{Chap05_SOR06.png}
    \caption{Norigami hat crease pattern and 3D shapes made by regular surface of revolution.}  
    \label{Fig:SOR06}
\end{figure} 

These gluing segments have the minimum gluing area, and the resulting flaps can be rotated to any angle (except multiples of $\pi,$) never having collisions with other segments or flap.  However, this division also generates 3 different gluing areas instead of 1, making the assembly process longer than before, if it is performed by hand.

\begin{figure}[h]
\centering
    \includegraphics[width=.95\linewidth]{Chap05_SOR05.png}
    \caption{Norigami hat crease pattern and 3D shapes made by independent gluing pattern.}  
    \label{Fig:SOR05}
\end{figure} 

We proposed to use the crease patterns in Fig.\ref{Fig:SOR06} in combination with the crease pattern in Fig.\ref{Fig:SOR04} and removing all the remaining paper material by cutting (see Fig. \ref{Fig:SOR05}). We called this new crease pattern as ``independent gluing pattern'', and can be selected in our software to be applied totally or partially depending on the requirements. 
Furthermore, the crease patterns created with this methodology (with or without independent gluing pattern) can be now automatically folded and glued by the proposed robot from Fig. \ref{Fig:Folding-Gluing}.

%------------------------------------------------------------------------------
% Results for SOR
%------------------------------------------------------------------------------
\section{Results}
\label{Sec:ResultsSOR}
To demonstrate the applicability of the proposed robot, three different shapes: a hexagonal box, a pod, and a hyperbolioid were assembled by the proposed robot. Their corresponding crease pattern, expected 3D shape, and final product are show in Fig.\ref{Fig:Results}.  All of these shapes were made setting $N=6$;

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=.95\linewidth]{Chap05_SORResults.png}
    \caption{Crease pattern examples and folded and opened figure. (a) Hexagonal box, (b) Hexagonal pod, (c) Hexagonal hyperboloid approximation.}
     \label{Fig:Results}
\end{center}
\end{figure} 
 
The complexity reduction is possible due to adding gluing areas into the crease pattern, which reduces movements in the robot by executing multiple tasks at the same time. Here, a manipulation is considered as a combination of movements required to execute a task, e.g. a fold, a turn over, a rotation, or a squash \cite{Tanaka2007}. Table.\ref{Table:NOM} expose the number of manipulations in a group of crease patterns folded by hand and the proposed robot.
 
\begin{table}
\begin{center}
	\caption{Number of manipulations.} 	
	\begin{tabular}{ p{0.29\linewidth} | p{0.20\linewidth}  | p{0.20\linewidth} | p{0.20\linewidth}   }
		Pattern Name & $\#$ manipulations Origami crease pattern   & $\#$ manipulations Norigami crease pattern  &  $\#$  of manipulations reduced \\
		
  		\hline			
  		Box (Fig.\ref{Fig:Results}a) & 31 & 31 & 0 \\
  		Pod (Fig.\ref{Fig:Results}b) & 67 & 31 & 36 \\
  		Hyperboloid (Fig. \ref{Fig:Results}c) & 37 & 31 & 6 \\
  		Hat (Fig.\ref{Fig:SOR04}) & 61 & 31 & 30 \\
  		\hline 
	\end{tabular}
	\label{Table:NOM}
\end{center}
\end{table}


%------------------------------------------------------------------------------
% Discussion Section
%------------------------------------------------------------------------------
\section{Discussions}
\label{Sec:DiscussionSOR}

As can be observed in fig.\ref{Fig:Results}, several 3D shapes can be created and successfully folded and assembled by the proposed robot using the proposed methodology. Due to the flexible properties of the paper, the final 3D shape has to be opened by hand adopting the desired shape. However, this process of opening the shape is difficult to execute in some cases, e.g. shapes that are completely closed, or figures with changes in the rotational direction of the angle $\theta$ like the one in Fig.\ref{Fig:SOR04}.

As can be observed in the area surrounded by the red circles in fig.\ref{Fig:SOR04}, there are some cases were the flaps can generate penetration in the paper. According to \cite{Mitani2009}, to avoid this problem all coordinates of the $y$ axis (i.e. the axis of rotation) of the vertices of the input profile have to be monotonically increasing or decreasing, i.e. $P_{(n-1,y)} \leq P_{(n,y)}  (1 \leq n<N)~or~ P_{(n-1,y)} \geq P_{( n,y)}  (1 \leq n<N)$. However, in our proposed methodology, new cutting lines can be included in the crease pattern reducing the area of the flaps, making possible to rotate them freely without collision (except with angles $\gamma$ multiples of $\pi$).

In table \ref{Table:NOM} we can observe the required manipulation to accomplish several 3D shapes using origami and the proposed robot. It can be observed that in the box example the number of manipulation in both cases is the same, this is because the crease pattern only has one stage with changes in the $x$ axis in its profile. This means that the complexity in manipulations is exponentially increasing by the number of stages plus one, if there is changes in the $x$ axis in any of the stages in the profile, i.e. $Num. ~of~ Manipulations = (3K+1)N+1; ~if (P_{(n-1,x)} \neq P_{(n,x)}~in~all~sub-segments)$ or $Num. ~of~ Manipulations = (3K-1)N+1; ~if (P_{(n-1,x)} = P_{(n,x)}~in~a~sub-segment)$, $N$ is the number of sub-segments, ans $K$ is the number of points in the poly-line.
 
%------------------------------------------------------------------------------
% Summary of the SOR chapter
%------------------------------------------------------------------------------
\section{Summary}
\label{Sec:SummarySOR}

In this chapter, a methodology to generate a crease patterns based on rotational surfaces has been proposed to construct several 3D paper models and  then they are built by the proposed robot. The crease patterns made with this methodology can be assembled by hand as well. Due to some limitations in the resulting flap locations, some new cutting lines have been added to the original origami pattern to permit them to move freely, even inside the 3D paper model. Furthermore, a new proposed crease pattern called ``independent gluing segment pattern'' has been proposed as a solution for very complex shapes with not monotonically increasing or decreasing in the angle $\theta_n$. Using the independent gluing segment pattern, previously problems such as overlapping between the flaps and the paper-body, or the possibility of introducing the flaps inside the paper's structure have been solved. Several shapes have been exposed to validate this method using a PC software developed in Matlab\textregistered. The results show that the number of manipulations are reduced, specially in crease patterns with several stages or number of sub-segments (see table \ref{Table:NOM}).

Although this methodology can be used to create patterns for being folded by a robot, all of them are based on surface of revolution. This means that only a limited number of patterns can be made by this methodology, and irregular shapes or complex structures cannot been achieved. In the chapter \ref{Ch06}, a generalization of the methodology presented in this chapter is proposed.


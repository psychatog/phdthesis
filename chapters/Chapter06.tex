% Chapter 6: Improve methodology for designing complex 3D shapes by the proposed robot
The methodology explained in the previous chapter \ref{Ch05}, can be used to create 3D shapes that are based on surface of revolutions (SOR), able to be built by the proposed robot. Although this methodology expands the applicability of the proposed robot, there are figures that are not based on SOR, because they have irregular shapes. Software like: PepaKura Designer, Tree-Maker, or Origamizer can be used to create these types of irregular figures as we can observe in fig. \ref{Fig:ComplexEx}.

\begin{figure}[h]
\centering
    \begin{tabular}{c}
    \includegraphics[width=.6\linewidth]{Chap06_ExA.png} \\
    \includegraphics[width=.6\linewidth]{Chap06_ExB.png} \\
    \includegraphics[width=.6\linewidth]{Chap06_ExC.png}
    \end{tabular}
    \caption{Examples of irregular shapes made by existing software. First row: TreeMaker. Second row: Origamizer. Third row: Pepakura Designer}  
    \label{Fig:ComplexEx}
\end{figure}

However, the resulting 2D crease patterns for these complex shapes are even more complicated to build than the SOR counterparts, having inside folds that required multiple manipulations at the same time, making extremely difficult to assemble these 3D shapes automatically using the robot. To solve irregular 3D shapes using the slightly modified version of our already existing robot, a novel methodology extracted from the previous SOR methodology is proposed and explained in this chapter. In the SOR methodology, a single profile is rotated in sections with an angle of $2\pi/N$. These sections generate $N$ equal sub-segments, side by side to each other. The idea with the methodology proposed in this chapter is to create a 2D pattern of shapes that have two or more different profiles. This new methodology can only be applied to figures with star-shaped-projected polyhedrons. \footnote{A star-shaped-projected polyhedron is a structure, that seeing from above, and performing vertical slices, all the slices have a star-shaped polygon form.} An example of a star-shaped polygon is shown in fig. \ref{Fig:SS01}. 

\begin{figure}[h]
\centering
    \includegraphics[width=.6\linewidth]{Chap06_Star-shapedEx.png} 

    \caption{Example of a star-shaped polygon.}  
    \label{Fig:SS01}
\end{figure}

A star-shaped-polygon is an irregular 2D polygon that contains a point $Z_n$, from which the entire boundary of the polygon is reachable, i.e. all vertex $P_{0,n}$, $\cdots$ , $P_{k-1,n}$, $P_{k,n}$, $P_{k+1,n}$, $\cdots$ , $P_{K,n}$, where K is the number of points in a height $n$ \cite{Preparata1985}.

The art-gallery theorem states that $\frac{K}{3}$ points are necessary to cover an $K$-gon, and in particular, this shows that polygons for which $3 \leq K \leq 5$ are necessarily star-shaped \cite{Aggarwal1984, Avis1981}. 
There are many ways to calculate the center $Z_n$. One of the most efficient algorithms is the one proposed by Lee and Preparata \cite{Lee1979}. However, in the tests performed in this chapter, we consider the points $Z_n$ to be previously known.

As previously said, this new methodology translates and performs modification into the SOR methodology explained in chapter \ref{Ch05}. One difference that can be noticed when irregular forms are analyzed is that they have two or more profiles, instead of the single profile of SOR counterpart. Therefore, a novel procedure has to be developed to convert 3D irregular shapes, into multiple profiles of a 2D crease pattern. In this chapter, a procedure to obtain the profiles is proposed and exposed. Then, some modifications to the profiles are performed, to be able to translate the spatial information into the 2D crease pattern. Next, the procedure to build the crease pattern and considerations for automation are given. Finally some examples are shown at the end of this chapter.

%------------------------------------------------------------------------------
% Technique for obtaining the profiles from a 3D model
%------------------------------------------------------------------------------
\section{Obtaining the profiles from a 3D model}

The first step in our methodology consists of obtaining and arranging all the possible profiles in a 3D model. These profiles are required to have the same number of points, and all these points have to be at the same height. Using the information of the vertices from the 3D model, the heights ($P_{k,n,z}=Z_{n,z}$) and rotation angles ($\alpha_k$) can be obtained using the following algorithm:

\begin{table}[h]
	\caption{Algorithm to obtain the heights and rotation angles from a 3D model}
	\label{Tab:ProfilesA}
	\centering
	\begin{tabular}{l}
	\hline
		Having a 3D model as Fig. \ref{Fig:SS02}a. \\
		\quad For each point $P_m$ in the 3D model: \\
		\qquad if ($Z_{n,z}$ and $\alpha_k$ are empty ): \\
			\qquad \quad $Z_{n,z}=P_{m,z}$; \\
			\\
			\qquad \quad $\alpha_k=tan^{-1}\frac{P_{m,y} - Z_{n,y}}{P_{m,x} - Z_{n,x}}$; \\
			\qquad \quad if ($\alpha_k < 0$): $\alpha_k=\alpha_k+2\pi$; \\
			\qquad \quad if ($\alpha_k \geq 2\pi$): $\alpha_k=\alpha_k-2\pi$; \\
			\qquad \quad $n++$; \\
			\qquad \quad $k++$; \\
		
		\qquad else:\\
			\qquad \quad if ($P_{m,z} \notin Z_{n,z}$): \\
				\qquad \qquad $Z_{n,z}=P_{m,z}$; \\
				\qquad \qquad $n++$; \\
				\\
			\qquad \quad $\beta=tan^{-1}\frac{P_{m,y} - Z_{n,y}}{P_{m,x} - Z_{n,x}}$; \\
			\qquad \quad if ($\beta < 0$): $\beta=\beta+2\pi$; \\
			\qquad \quad if ($\beta \geq 2\pi$): $\beta=\beta-2\pi$; \\
			\qquad \quad if ($\beta \notin \alpha$): \\
				\qquad \qquad $\alpha_k=\beta$; \\
				\qquad \qquad $k++$; \\
	\hline
	\end{tabular}
\end{table}

\begin{figure}[h]
\centering
    \begin{tabular}{cc}
    \includegraphics[width=.45\linewidth]{Chap06_Star-shapedA.png} &
    \includegraphics[width=.45\linewidth]{Chap06_Star-shapedB.png} \\
    (a) & (b)
    \end{tabular}
    \caption{Example of a star-shaped projected polyhedron. (a) Isometric view of the polyhedron. (b) Top view and projections of the planes in 2D.}  
    \label{Fig:SS02}
\end{figure}

In these profiles the maximum height $Z_{max}$ and minimum height $Z_{min}$ have to be the same for all profiles. Also, the sum of all the angles in $\alpha_k$ have to be equal to $2\pi$.
After all the heights and rotational angles have been extracted from the 3D model, the next step is to obtain the profiles. To obtain these profiles, the procedure for obtaining the intersection between a plane and the exterior of the 3D shape is used. The cut planes are created using the information of the heights and angles previously calculated. The resulting cut planes and profiles from the example in Fig. \ref{Fig:SS02}, can be observed in Fig. \ref{Fig:SS03}.

\begin{figure}[h]
\centering
    \begin{tabular}{cc}
    \includegraphics[width=.45\linewidth]{Chap06_SS_CutPlanes.png} &
    \includegraphics[width=.45\linewidth]{Chap06_SS_Profiles.png} \\
    (a) & (b)
    \end{tabular}
    \caption{Resulting profiles after extraction by plane cuts. (a) Isometric view of the polyhedron with the corresponding plane cuts. (b) Resulting profiles. Yellow: profile 1, green: profile 2, blue: profile 3, and orange: profile 4.}  
    \label{Fig:SS03}
\end{figure}

It can be observed from Fig. \ref{Fig:SS03}b, that not all the poly-lines have the same number of points. The profile number 1 (yellow) and 2 (green) have two points. On the other hand, the profile number 3 (blue) and 4 (orange) have three points, all them with a equal height. In this chapter, an algorithm to add missing points into the poly-lines is proposed. First, the algorithm eliminates any redundant points using the algorithm in Table. \ref{Tab:QuitPoints}. Then the algorithm in Table. \ref{Tab:AddPoints} uses the information of the missing heights to add missing points into the poly-lines that required it. After the profiles are completed, they are arranged in a matrix of $n \times k$, where $n$ is the number of points on each profile, and $k$ is the number of profiles.

\begin{table}[h]
	\caption{Algorithm to eliminate redundancies in poly-lines}
	\label{Tab:QuitPoints}
	\centering
	\begin{tabular}{l}
	\hline
		\quad Variable to count the number of points per height: $m=1$;  \\
		\quad For each point $P_n$ on a profile $k$: \\
			\qquad if ($n==1$): \\
				\qquad \quad $P_{n}^f+=P_{n}$; \\
				\qquad \quad $m++$; \\
	
			\qquad else:\\
				\qquad \quad if ($P_{n-1,z} \neq P_{n,z}$): \\
					\qquad \qquad $P_{n}^f+=P_n$; \\
					\qquad \qquad $m=1$; \\
					\qquad \qquad $n++$; \\
				\qquad \quad else: \\
					\qquad \qquad $m++$; \\
					\qquad \qquad if($m < 3$):\\
						\qquad \qquad \quad $P_{n}^f+=P_{n}$; \\
					 	\qquad \qquad \quad n++; \\
					\qquad \qquad else: \\
						\qquad \qquad \quad $P_{n-1}^f+=P_{n}$;\\
		\quad Return: $P^f$;  \\

	\hline
	\end{tabular}
\end{table}

\begin{table}[h]
	\caption{Algorithm to add missing points into the poly-lines}
	\label{Tab:AddPoints}
	\centering
	\begin{tabular}{l}
	\hline
	Having a vector with all heights: $H_n$:  \\
	For each point $P_n$ on a profile $k$: \\
		\quad Vector to save the new indexes: $i=\left [ ~ \right]$; \\
		\quad For each height $h$ in $H_n$: \\
			\qquad if($P_{n,z} \notin H_n$): \\
				\qquad \quad if($h>P_{n,z}$): \\
					\qquad \qquad $L=P_{n}- P_{n+1}$\\
					\qquad \qquad $P_{n,x}^t=\frac{h-P_{n,z}L_x}{L_z}+P_{n,x}$;\\
					\qquad \qquad $P_{n,y}^t=\frac{h-P_{n,z}L_y}{L_z}+P_{n,y}$;\\
					\qquad \qquad $P_{n,z}^t=h$;\\
					\qquad \qquad $i+=n$; \\
				\qquad \quad else: \\
					\qquad \qquad $L=P_{n}- P_{n-1}$;\\
					\qquad \qquad $P_{n,x}^t=\frac{h-P_{n,z}L_x}{L_z}+P_{n-1,x}$;\\
					\qquad \qquad $P_{n,y}^t=\frac{h-P_{n,z}L_y}{L_z}+P_{n-1,y}$;\\
					\qquad \qquad $P_{n,z}^t=h$;\\
					\qquad \qquad $i+=n-1$; \\
					\\
		\quad for each index $t$ in $i$:\\
			\qquad $P_{n}^f=P_{n:i}+P_{i}^t+P_{i:end}$; \\
			\\
		\quad Return: $P_n^f$; \\

	\hline
	\end{tabular}
\end{table}

%------------------------------------------------------------------------------
% Create the crease patterns from profiles
%------------------------------------------------------------------------------
\section{Creating the 2D crease pattern from the profiles}

After the information of the profiles is stored in the previously mentioned matrix, and the angle vector $\alpha_k$ from Table \ref{Tab:ProfilesA}, the next step is to use that information to create the 2D crease pattern.
For the following example in Fig. \ref{Fig:SS03}, we are going to assume a pair of different profiles, with the same number of points, and the number of segments is $K=6$. This means that each profile is repeated 3 times intercalating between them, and the angle $\alpha_k$ between profiles is constant and equal to $pi/3$.

\begin{figure}[h]
\centering
    \begin{tabular}{cc}
    \includegraphics[width=.45\linewidth]{Chap06_SS_ExA.png} &
    \includegraphics[width=.45\linewidth]{Chap06_SS_ExB.png} \\
    (a) & (b)
    \end{tabular}
    \caption{Example to build a general crease pattern with two or more profiles. (a) Profiles. (b) Crease pattern equivalents. Dashed line: first profile, continuous line: second profile, gray areas: gluing areas.}  
    \label{Fig:SS03}
\end{figure}

As an starting point, the distances $l_n$ and the widths $w_{k,n}$ between all points from a single pair of profiles have to be calculated as follows:

\begin{equation}
	l_{k,n}=\vert\vert P_{k,n+1}-P_{k,n} \vert\vert.
	\label{Eq:SS01}
\end{equation}

\begin{equation}
	w_{k,n}=\sqrt{P_{k,n,x}^2+P_{k+1,n,x}^2-2P_{k,n,x}P_{k+1,n,x}cos\alpha_k}.
	\label{Eq:SS02}
\end{equation}


After we calculate the widths and distances from equations (\ref{Eq:SS01}) and (\ref{Eq:SS01}), we perform an adaptation of the SOR methodology using the new information obtained previously to calculate the $P'_{k,n}$ from Fig. \ref{Fig:SS03}. In this new case, for each point in a pair of profiles $P_{k,n}; P_{k+1,n}$ we obtain a single point in the crease pattern, instead of the double point obtained in the previous SOR methodology. 


\begin{equation}
	W_k=max(w_{k,n}).
	\label{Eq:SS03}
\end{equation}

\begin{equation}
	\begin{array}{lr}
	P1'_{k,n,x}=\frac{W_k - w_{k,n}}{2}; & P2'_{k,n,x}=\frac{W_k + w_{k,n}}{2}.
	\end{array}
	\label{Eq:SS04}
\end{equation}

\begin{equation}
	P1'_{k,n,y}= \left \{\begin{array}{lr}
	0, & if(n=0); \\
	P1'_{k,n-1,y} + \sqrt{ l_{k,n}^{2}- \left( \frac{w_{k,n}-w_{k,n-1}}{2} \right) ^{2} }, 	 & if (n>0).
	\end{array} \right.
	\label{Eq:SS05}
\end{equation}

\begin{equation}
	P2'_{k,n,y}= \left \{\begin{array}{lr}
	0, & if(n=0); \\
	P2'_{k,n-1,y} + \sqrt{ l_{k+1,n}^{2}- \left( \frac{w_{k+1,n}-w_{k+1,n-1}}{2} \right) ^{2} }, 	 & if (n>0).
	\end{array} \right.
	\label{Eq:SS06}
\end{equation}

The resulting crease pattern made by using equations (\ref{Eq:SS01})-(\ref{Eq:SS06}), generate an approximation of the 3D object after it is assembled. The assembled shape has an error that depends on the desired shape angle differences. This produce some differences in the lengths of the 3D shape, made to guarantee symmetry in the gluing areas. This symmetry is necessary for executing the automation process using the proposed robot.

%------------------------------------------------------------------------------
% Results
%------------------------------------------------------------------------------
\section{Results}

For these experiments, three different shapes are used to analyze the proposed methodology. The first two shapes are irregular shapes that not require to add any points into their profiles. For the third example an irregular side hat is translated into a crease pattern. For this hat, both the redundant point elimination and the adding algorithms are used.

\begin{figure}[h]
\centering
    \begin{tabular}{cccc}
    \includegraphics[width=.24\linewidth]{Chap06_ResultsA1.png} &
    \includegraphics[width=.24\linewidth]{Chap06_ResultsA2.png} &
    \includegraphics[width=.24\linewidth]{Chap06_ResultsA3.png} &
    \includegraphics[width=.24\linewidth]{Chap06_ResultsA4.png} \\
    
    \includegraphics[width=.24\linewidth]{Chap06_ResultsB1.png} &
    \includegraphics[width=.24\linewidth]{Chap06_ResultsB2.png} &
    \includegraphics[width=.24\linewidth]{Chap06_ResultsB3.png} &
    \includegraphics[width=.24\linewidth]{Chap06_ResultsB4.png} \\
    
    \includegraphics[width=.24\linewidth]{Chap06_ResultsC1.png} &
    \includegraphics[width=.24\linewidth]{Chap06_ResultsC2.png} &
    \includegraphics[width=.24\linewidth]{Chap06_ResultsC3.png} &
    \includegraphics[width=.24\linewidth]{Chap06_ResultsC4.png} \\
    \end{tabular}
    \caption{Experiments carried out with the multiple profiles methodology. From left to right: Original STL 3D model, extracted profiles, crease pattern, and the approximation.}  
    \label{Fig:SS04}
\end{figure}

Figure \ref{Fig:SS04} shows three different examples of 3D shapes that can be made using the multiple-profiles methodology proposed in this chapter. The first shape is a star-shaped structure with two different profiles, for this case $K=6$ and $N=2$, so any points are needed to be added or removed. Also, due to intercalating between profiles, the angle $\alpha_k$ is constant and has a value of $\pi/K$.

The shape in the second row is a pyramid-like structure with three different profiles. For this case $K=3$ and $N=2$, and the angle between each profile $\alpha_k$ varies between each profile. Equal to the first shape, any points have to be added or removed. 

The last shape is a hat, similar to the one made in the previous chapter \ref{Ch05}, but if it is analyzed carefully there are some differences. The main difference can be observed between the second and the third point in the poly-line; where the third line is rotated $\pi/K$rad. This generates a turning effect that can be used later to compact the hat in a flat state (see Fig.\ref{Fig:SS07}). This rotation generates a new profile, as can be observed in the second column of Fig. \ref{Fig:SS04}. For this case, points have to be removed and added to complete the poly-lines. The added points can be observed as red dots in the first column image of Fig \ref{Fig:SS04}. The number of sub-segments $K=12$ and $N=6$. The rotation angle is constant and has a value of $\alpha_k=\pi/K$. Although the approximation is very close to the original figure, the transverse line, that generates the characteristic of compacting the hat is not present. 

%------------------------------------------------------------------------------
% Discussions
%------------------------------------------------------------------------------
\section{Discussions}
The methodology proposed in this chapter focuses on the creation of more diverse 3D shapes, not limited to SOR. The main idea is to use this methodology in an automatic folding robot, doing as less modification as possible to the mechanisms that already exist. The results exposed in Fig. \ref{Fig:SS04} show that several irregular-side shapes can be achieved by the proposed method. This technique is an enhanced version of our previous work, exposed in chapter \ref{Ch05}. Although the shapes are not geometrically perfect, they are a very close approximations to the real objects. The rule of the gradually increases or decreases in the poly-lines points also applies into this methodology (see more info on chapter \ref{Sec:DiscussionSOR}). However, in this case all the profiles have to achieve this rule. A good solution is to always apply the total independent gluing segment variation, explained in chapter \ref{Sec:TIGS}. This allows us to freely allocate the generated flaps and used the minimal gluing area.

\begin{figure}[h]
\centering
    \begin{tabular}{cc}
    \includegraphics[width=.45\linewidth]{Chap06_Perpendicular_Problem01.png} &
    \includegraphics[width=.45\linewidth]{Chap06_Perpendicular_Problem02.png} \\
    (a) & (b)
    \end{tabular}
    \caption{Lines in the profile to be avoided. (a) Isometric of a tetrahedron with edges perpendicular to the pivot axis. (b) Profiles of the tetrahedron. The red Lines are perpendicular to the pivot axis.}  
    \label{Fig:SS06}
\end{figure}

There is a special case within applying this methodology that has to be avoided, and its exposed in Fig. \ref{Fig:SS06}. In this case there is an edge of the 3D model that is perfectly perpendicular and over a radius line. The problem is caused at the moment of counting the points that compose a poly-line. Due to have multiple points in a single height, the reduction algorithm eliminates the equal points, and the adding point algorithm cannot found the border points that compose a line to create new points. To solve this problem it is recommended to rotate the 3D model to a more stable state. An stable state is where a face is perpendicular to the ground or parallel to the pivot axis.


\begin{figure}[h]
\centering
    \begin{tabular}{cccccc}
    \includegraphics[width=.14\linewidth]{Chap06_Hat_Down01.png} &
    \includegraphics[width=.14\linewidth]{Chap06_Hat_Down03.png} &
    \includegraphics[width=.14\linewidth]{Chap06_Hat_Down03.png} &
    \includegraphics[width=.14\linewidth]{Chap06_Hat_Down04.png} &
    \includegraphics[width=.14\linewidth]{Chap06_Hat_Down05.png} &
    \includegraphics[width=.14\linewidth]{Chap06_Hat_Down06.png} 

    \end{tabular}
    \caption{Closing the origami hat into a flat state.}  
    \label{Fig:SS07}
\end{figure}

The hat shape shown in Fig. \ref{Fig:SS04} has a transverse crease line that allows the figure to be compacted into a flat state, as can be observed in Fig. \ref{Fig:SS07}. Due to the flaps generated in the gluing process, the open-close behavior is very difficult to be executed by applying this methodology. However, making the traverse lines, and folding them by hand, could create figures that are flat-foldable partially.

%------------------------------------------------------------------------------
% Summary MPM
%------------------------------------------------------------------------------
\section{Summary}
In this chapter, an enhanced version of the methodology exposed in chapter \ref{Ch05} has been proposed and explained. Several examples have been shown to explain the methodology procedure. Proper adaptations of the SOR methodology have been made in order to be applied into star-shaped projected polyhedrons. As is explained, the resulting analysis generates multiple profiles that later are used to create the 2D crease pattern. Although these crease patterns have irregular shapes, the gluing areas are symmetrical, but not congruent. This means that multiple stamps have to be used in the proposed machine, i.e. one for each profile.

The resulting 3D shapes are in the majority static and cannot be moved as the model in Fig. \ref{Fig:SS06}. However, performing the proper crease lines by hand, and to reduce the gluing area applying the independent gluing segment methodology, can help to create semi-flat foldable structures.


%After we calculate the widths and distances from equations (\ref{Eq:SS01}) and (\ref{Eq:SS01}), we have to decide the reference line. This line could be any horizontal line with magnitude equal to any of the $w_{k,n} \neq 0$, and is gonna be parallel to the $x$ axis in the crease pattern. For the example in Fig. \ref{Fig:SS03}b, we chose the line with magnitude $w_{k,1}$ as the reference line. Apart of this, we have to extract one angle, denoted here as $\rho_{k,n}$ from the information in the 3D model, and calculated as follows:
%
%\begin{equation}
%	\rho_{k,n}=cos^{-1} \left( \frac{(P_{k+1,n}^R-P_{k,n}^R) \cdot (P_{k,n+1}^R-P_{k,n}^R)}{w_{k,n} \cdot l_{k,n}} \right).
%	\label{Eq:SS03}
%\end{equation}
%where $(-)^R$ denotes the position of that specific point in space.
%
%Using the information from equations (\ref{Eq:SS01})-(\ref{Eq:SS03}), the coordinates of the crease pattern $P'_{k,n}$ in Fig. \ref{Fig:SS03} can be calculated as follows:

%\begin{equation}
%	P1'_{x}= \left \{ \begin{array}{lr}
%		0; & n=0. \\
%		l_{k,n}cos \left( \phi_{k,n} + \rho_{k,n} \right); & n>0.
%	\end{array} \right .
%	\label{Eq:SS04}
%\end{equation}
%
%\begin{equation}
%	P2'_{x}= \left \{ \begin{array}{lr}
%		w_{k,n}; & n=0. \\
%		w_{k,n} + l_{k+1,n}cos \left( \pi +\phi_{k,n} - \rho_{k,n} \right); & n>0.
%	\end{array} \right .
%	\label{Eq:SS05}
%\end{equation}
%
%\begin{equation}
%	\phi_{k,n}= cos\frac{P'_{k+1,n,x}-P'_{k,n,x}}{w_{k,n}}.
%	\label{Eq:SS06}
%\end{equation}
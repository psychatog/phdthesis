% Chapter 3: Origami-performing robot design

%---------------------------------------------------------------------
% Introduction to the chapter
%---------------------------------------------------------------------
In this chapter, the development of a folding-robot is described. Previous works in origami-performing robots \cite{Balkcom2008, Yao2007, Kihara2010, Namiki2003} have showed that the required manipulations in paper-folding can be a challenge. The robots used on these works use complex robot systems, or adjust standard robot manipulators that exist in the market to fold paper sheets. However, these existing robot devices have complex mechanisms and are expensive. In order to reduce the number of manipulations in the folding process, the use of simple folding procedure combined with gluing areas is proposed in this dissertation. The use of LEGO MINDSTORMS NXT\textregistered is also proposed to reduce the price of the system, and give us an easy way to analyze and test different concepts during the development process. 

Recent studies demonstrate extended use of Lego MINDSTORMS NXT\textregistered~ for educational purposes \cite{Klassner2002}. Most of them have been used for control designs \cite{Axelsson2011}, signal processing \cite{Berglund2010} and manipulation \cite{Yamamoto2008}. The work in \cite{Yamamoto2008}, details the development of a model for a mobile robot constructed from LEGO MINDSTORMS NXT \textregistered. 

Figure \ref{Fig:Folding-Gluing} shows the proposed robot from this dissertation. First, to create a 3D shape using this robot, the desired crease pattern has to be designed using a design methodology based on a surface of revolution (SOR) technique~\cite{Mitani2009} explained later in chapter \ref{Ch05}. After the crease pattern is created, the robot proceed to folds the paper sheet. Mechanical information such as widths between folds and the shape of the gluing stamp are extracted from the pattern design process. The folding process of the proposed robot is explained in chapter \ref{Ch03:Trajectory}, and is divided into two stages: the first one (F), consists of a pre-folding process that creates the crease lines of the folding pattern. At the second stage (G), two processes of a gluing and a folding process are performed at the same time, and it generates an accordion shape that adopts different forms after opened.

To represent the functional behavior of the proposed robot, the kinematic model, dynamic model, and the control scheme have been calculated (To see the dynamic calculations refer to chapter \ref{Ch04}). In this chapter, the trajectories calculations are exposed, this includes, the kinematic model, and some trajectory considerations to implement the proposed robot into mass-production procedures.  Then, several algorithms related with the paper properties such as: spring-back and stacking effects, are included into the trajectories generation algorithms to improve the accuracy of the movements. Finally, several results are exposed and some conclusions are summarized.

%---------------------------------------------------------------------
% Trajectory generation
%---------------------------------------------------------------------
\section{Trajectory generation}
\label{Ch03:Trajectory}

\subsection{Extraction of the required manipulations}
\label{Ch03:Extraction}

In order to extract the required manipulations in a regular folding process, a procedure executed by a person to create a folding-gluing object is analyzed. This object is designed with the methodology exposed in chapter \ref{Ch05} and has an spherical shape.
It is observed, that the person requires guide-lines to facilitates the folding of the paper and applying the glue properly . Figure \ref{Fig:Handmade} shows the procedure executed by hand for both, folding and gluing processes.

\begin{figure}[h]
  \begin{center}
  \begin{tabular}{ccccc}
   \includegraphics[width=0.170\textwidth]{Chap03_Handmade_A.jpg} &
   \includegraphics[width=0.170\textwidth]{Chap03_Handmade_B.jpg} &
   \includegraphics[width=0.170\textwidth]{Chap03_Handmade_C.jpg} &
   \includegraphics[width=0.170\textwidth]{Chap03_Handmade_D.jpg} &
   \includegraphics[width=0.170\textwidth]{Chap03_Handmade_E.jpg} \\
   \includegraphics[width=0.170\textwidth]{Chap03_Handmade_F.jpg} &
   \includegraphics[width=0.170\textwidth]{Chap03_Handmade_G.jpg} &
   \includegraphics[width=0.170\textwidth]{Chap03_Handmade_H.jpg} &
   \includegraphics[width=0.170\textwidth]{Chap03_Handmade_I.jpg} &
   \includegraphics[width=0.170\textwidth]{Chap03_Handmade_J.jpg} 
   \end{tabular}
    \caption{Handmade procedure to create a folding-gluing sphere.}
     \label{Fig:Handmade}
\end{center}
\end{figure} 

To create a 3D object by hand several procedures have to be accomplished. It can be observed in Fig.\ref{Fig:Handmade}:A and \ref{Fig:Handmade}:B that a person usually performs a smooth folding, and then executes a refinement of the same folding by tracing the previously scratched crease line with the fingertips. Then, the person repeats these two processes multiple times and turns the paper 180 degrees to intercalate between valley and mountain folds (Fig.\ref{Fig:Handmade}:C-G). In the gluing process (Fig.\ref{Fig:Handmade}:H-J), applying the glue to the paper may be difficult to be executed by hand, depending on the size or shape of the pattern's gluing area. This gluing process can be made in different ways. Despite this, it is noticed that the paper, after been pasted, has to be held for certain time(like in Fig.\ref{Fig:Handmade}:J), because the glue takes some time to get dry. 
Taking in account all the information obtained from Fig.\ref{Fig:Handmade}, an extraction of requirements and possible solutions are generated and exposed in Table.\ref{Tab:Req} .

\begin{table}[h]
	\caption{Requirements and possible solutions.}
	\label{Tab:Req}
	\begin{tabular}{p{0.45\linewidth} p{0.45\linewidth}}
	\bf Requirements & \bf Possible Solutions\\
	\hline
	\multirow{2}{1.0\linewidth}{The paper has to be folded in both directions, valley and mountain fold directions.} & 
		$\bullet$Turn the paper 180$^{\circ}$ using a hand mechanism or manipulator. \\ &
		$\bullet$ Use two mechanisms, one for valley folds and another for mountain folds. \\
	\hline
	The glue has to be applied as precisely as possible. & 
		$\bullet$ Use a stamp with the desired shape. \\
	\hline
	The paper has to be held after glued. & 
		$\bullet$ Use a mechanism to hold the paper. \\
	\hline
	\end{tabular}
\end{table}

%---------------------------------------------------------------------
% Robotic Manipulations
%---------------------------------------------------------------------
\subsection{Robotic manipulations}
\label{Ch03:RoboticM}

\begin{figure}[h]
  \begin{center}
  \begin{tabular}{p{0.5\textwidth} p{0.5\textwidth}}
   \multicolumn{2}{c}{ \includegraphics[width=0.7\linewidth]{Chap03_LEGO_Robot.png} } \\
    \hline
    F1: Pulling roller
    r(NXT Servomotor) & G1: Rack-pinion forward-backward system(NXT Servomotor) \\ 
    F2: Superior folding handle(NXT Servomotor) & G2: Rack-Pinion up-down system(NXT Servomotor) \\
    F3: Inferior folding handle(NXT Servomotor) & G3: Pressing device and stamp location \\
    F4: Advancing sensor(Tachometer) & G4: Glue-distribution system \\
    F5: Folding Lego NXT Brain-Brick & G5: Gluing sensor(Tachometer) \\
    F6: Folding Base  & G6: Holding fingers(NXT Servomotor) \\
    ~ & G7: Gluing NXT Brain-Brick \\
     \\ \hline
    \end{tabular}
    \caption{Proposed robot system (F) Folding step (G) Gluing step.}
     \label{Fig:Folding-Gluing}
     \end{center}
\end{figure}

The proposed robot has two separately stages. In the first stage, called folding stage(Fig.\ref{Fig:Folding-Gluing}:F) the robot performs scratches of the desired crease lines alterning between valley and mountain folds. After the paper is folded in this folding stage, it is begins the folding-gluing stage (Fig.\ref{Fig:Folding-Gluing}:G). To create the trajectories of this stage, information from previous folding step is used. Using the information of the output widths from the folding stage, the spring-back effect can be estimated and used to generate the trajectory of the folding-gluing stage. This value is calculated using the mechanical properties of the paper (i.e. paper type, and thickness), and the real values of the widths obtained from the advancing sensor(Fig.\ref{Fig:Folding-Gluing}:F4) in the folding stage. Both trajectories are loaded into the corresponding NXT Brain-Bricks (Fig.\ref{Fig:Folding-Gluing}:F5 and G7), the information of the first brick is transmitted into the second brick through Bluetooth communication.

To fold a 3D shape using the proposed robot, the following steps have to be done (see Fig.\ref{Fig:FoldingP}):

\begin{figure}[h]
  \begin{center}
  \begin{tabular}{cccc}
   \includegraphics[width=0.210\linewidth]{FoldingProcess_A.png} &
   \includegraphics[width=0.210\linewidth]{FoldingProcess_B.png} &
   \includegraphics[width=0.210\linewidth]{FoldingProcess_C.png} &
   \includegraphics[width=0.210\linewidth]{FoldingProcess_D.png} \\
   \includegraphics[width=0.210\linewidth]{FoldingProcess_E.png} &
   \includegraphics[width=0.210\linewidth]{FoldingProcess_F.png} &
   \includegraphics[width=0.210\linewidth]{FoldingProcess_G.png} &
   \includegraphics[width=0.210\linewidth]{FoldingProcess_H.png} \\
   \includegraphics[width=0.210\linewidth]{FoldingProcess_I.png}  &
   \includegraphics[width=0.210\linewidth]{FoldingProcess_J.png} &
   \includegraphics[width=0.210\linewidth]{FoldingProcess_K.png} &
   \includegraphics[width=0.210\linewidth]{FoldingProcess_L.png} 
   \end{tabular}
    \caption{Automatic folding-gluing procedure (See the details of each step A-L in the text)}
     \label{Fig:FoldingP}
\end{center}
\end{figure} 

\begin{itemize}

\item[Step-A]:  The machine starts with a sheet of paper inside the tray of the robot. Then the pulling roller~(Fig.\ref{Fig:Folding-Gluing}:F1) pushes the paper forward until the edge of the paper is in the desired position.  
 
\item[Step-B]: The inferior handle (Fig.\ref{Fig:Folding-Gluing}F3) rotates and presses the paper against the folding base generating a valley crease fold. The turning angle in the folding base is set to be 45 degrees in order to generate sharp crease lines without cutting the paper. 
 
\item[Step-C]: The inferior handle is released and returned to its initial position, letting the paper to move forward. Note that the spring-back effect in the paper keeps the paper folded with an angle of more than 90 degrees. 
 
\item[Step-D]: The pulling roller continues to push the paper forward until the edge of the paper is in the desired position.
 
\item[Step-E]: The superior handle~(Fig.\ref{Fig:Folding-Gluing}:F2) rotates and presses the paper against the folding base generating a mountain fold.
 
\item[Step-F]: The superior handle is released and returned to its initial position, letting the paper to move forward. The steps A-F are repeated $N-1$ times. At the end, the pulling roller pushes the paper forward to allocate the paper in the second stage.
 
%\item[Step-G]:  

\item[Step-G]:  In the second stage, the paper starts with one edge against the bottom wall located in the glue-distribution system (Fig.\ref{Fig:Folding-Gluing}:G4).  In Fig.\ref{Fig:FoldingP}H-N, the glue-distribution system is represented by a rectangular shape covered with a green semi-circled roller. The forward-backward system (Fig.\ref{Fig:Folding-Gluing}:G1) is represented with an "L-shape" and the two holding fingers (Fig.\ref{Fig:Folding-Gluing}:G6) are represented with a "T-shape". 

\item[Step-H]:  The forward-backward system ("L-shape") is moved backward using four rack-pinion mechanisms, forcing the gluing stamp (Fig.\ref{Fig:Folding-Gluing}:G3) to pass over the roller of the glue-distribution system (Fig.\ref{Fig:Folding-Gluing}:G4) to impregnate the stamp with glue. The forward-backward system stops at a distance computed from the combination of two factors: the spring-back effect in the paper and the accumulation of the paper forming the accordion shape after the paper is folded and glued.

\item[Step-I]: Then, the stamp is pressed against the paper using other four rack-pinion mechanisms (Fig.\ref{Fig:Folding-Gluing}:G2), impregnating the paper with glue.
 
\item[Step-J]: While the paper is held to the bottom base, the pressing device is moved forward until it reaches the bottom wall.
 
\item[Step-K]: Then the paper is fixed to the wall using the holding fingers (Fig.\ref{Fig:Folding-Gluing}:G6).
 
\item[Step-L]: The pressing device (Fig.\ref{Fig:Folding-Gluing}:G3) is released from the paper and returned to its initial position.
 
%\item[Step-N]: 
\end{itemize} 

Finally, process from G to L is repeated $N-1$ times until the paper adopts an accordion shape. The geometry of the final shape is deformed mainly in steps I and K. In step I, the stopping position of the forward-backward system must be accurately calculated to avoid pushing the stamp on the top of a previously folded line. In step J, previous glued and folded segments of the paper are accumulated to the wall and this should be accurately calculated the displacement of the forward-backward system.  
%---------------------------------------------------------------------
% Robot Kinematics
%---------------------------------------------------------------------
\subsection{Kinematic model of the robot}
\label{Ch03:Kinematics}

One of the most important parts to understand a robot behavior is to know its kinematic properties. The kinematics studies the movement of a robot inside a reference frame. The kinematics is an analytic description of the movement of the links of the robot in time, and particularly studies the relationship between the position and the orientation of the final point of the robot and the coordinates that this point takes in time. There are two different kinematic models to understand, the forward kinematic model (FKM) and the inverse kinematic model (IKM). 

The FKM obtains the spatial coordinates and orientation of the final point of the robot, using the length of the links and angles of rotation of these links. To perform this task a method called Denavit-Hatemberg parameters method, is usually used for reducing the computations. On the other hand, the IKM, obtains the angles of each link of the robot using the initial and final Cartesian position of the final point of the robot. To solve the IKM, the trigonometric method (also known as geometric method) is recommended. It has to be noticed that IKM may have multiple solutions, depending on the number of links in the robot \cite{Spong2006}.

%---------------------------------------------------------------------
% Trajectory interpolation
%---------------------------------------------------------------------
\subsection{Trajectory interpolation}
\label{Ch03:Interpolation}

It is recommended in a trajectory following, to perform smooth movements on each preset point to reduce the effects of mechanical noises (e.g. gear backslash, or abrupt overshoots). To accomplish this, a third grade interpolator (a.k.a cubic interpolator) is used \cite{Spong2006}. Having two points in space $\theta_{(t_0)}$, ~$\theta_{(t_f)}$, using the following equations and dividing the trajectory between these two points in a number of samples, we have:

\begin{equation}
	\theta_{(t)}=a_0+a_1 t+a_2 t^2+a_3 t^3.
	\label{Eq:Int1}
\end{equation}

\begin{equation}
	\dot \theta_{(t)}=a_1+2a_2 t+3a_3 t^2.
	\label{Eq:Int2}
\end{equation}
where $a_0$,~$a_1$,~$a_2$, and $a_3$ must be found to solve the interpolator between boundaries. We should notice that the values of $\theta_{(t)}$~ and $ \dot \theta_{(t)}$ are known for both times $t_0$ and $t_f$. 
\begin{equation}
	\theta_{(t_0)}=a_0+a_1 t_0+a_2 t_0^2+a_3 t_0^3.
	\label{Eq:Int3}
\end{equation}
\begin{equation}
	\dot \theta_{(t_0)} ̇=a_1+2a_2 t_0+3a_3 t_0^2.
	\label{Eq:Int4}
\end{equation}
\begin{equation}
	\theta_{(t_f)}=a_0+a_1 t_f+a_2 t_f^2+a_3 t_f^3.
	\label{Eq:Int5}
\end{equation}
\begin{equation}
	\dot \theta_{(t_f)} ̇=a_1+2a_2 t_f+3a_3 t_f^2.
	\label{Eq:Int6}
\end{equation}

Equations (\ref{Eq:Int3})-(\ref{Eq:Int6}) can be represented in a matrix way such as:

\begin{equation}
	\begin{array}{llcl}
	\left [ \begin{array}{cccc} 
	1 & t_0 & t_0^2 & t_0^3 \\
	0 & 1 & 2t_0 & 3t_0^2 \\
	1 & t_f & t_f^2 & t_f^3 \\
	0 & 1 & 2t_f & 3t_f^2
	\end{array}  \right ] & 
	\left [ \begin{array}{c} 
	a_0 \\
	a_1\\
	a_2 \\
	a_3
	\end{array} \right ]&
	= &
	\left [ \begin{array}{c} 
	\theta_0 \\
	\dot \theta_0\\
	\theta_f \\
	\dot \theta_f
	\end{array} \right ].
	\label{Eq:Int7}
	\end{array}
\end{equation}

The simple solution for  eq. (\ref{Eq:Int7}), is considering the initial and final velocities to being equal to zero. Suppose we have $t_0=0$, and $t_f=1~sec$, with $\dot \theta_0=0$, and $\dot \theta_f=0$. Replacing these values in equation (\ref{Eq:Int7}) we obtain:

\begin{equation}
	\begin{array}{llcl}
	\left [ \begin{array}{cccc} 
	1 & 0 & 0 & 0 \\
	0 & 1 & 0 & 0 \\
	1 & 1 & 1 & 1 \\
	0 & 1 & 2 & 3
	\end{array}  \right ] & 
	\left [ \begin{array}{c} 
	a_0 \\
	a_1\\
	a_2 \\
	a_3
	\end{array} \right ]&
	= &
	\left [ \begin{array}{c} 
	\theta_0 \\
	0\\
	\theta_f \\
	0
	\end{array} \right ].
	\label{Eq:Int8}
	\end{array}
\end{equation}

From Eq.\ref{Eq:Int7} we have the following four equations:

\begin{equation}
	a_0=\theta_0.
	\label{Eq:Int9}
\end{equation}
\begin{equation}
	a_1=0.
	\label{Eq:Int10}
\end{equation}
\begin{equation}
	a_2+a_3=\theta_f-\theta_0.
	\label{Eq:Int11}
\end{equation}
\begin{equation}
	2a_2+3a_3=0.
	\label{Eq:Int12}
\end{equation}
that later we get the following equations:

\begin{equation}
	a_2=3 \left( \theta_f-\theta_0 \right).
	\label{Eq:Int13}
\end{equation}
\begin{equation}
	a_3=-2 \left( \theta_f-\theta_0 \right).
	\label{Eq:Int14}
\end{equation}

Then, replacing all values of $a_0$ to $a_3$ into equations (\ref{Eq:Int1}) and (\ref{Eq:Int2}), the representative polynomial function of the cubic interpolator is obtained as:

\begin{equation}
	\theta_{(t)}=\theta_0 + 3\left( \theta_f-\theta_0 \right)t^2 - 2\left( \theta_f-\theta_0 \right)t^3.
	\label{Eq:Int15}
\end{equation}
\begin{equation}
	\dot \theta_{(t)}=6\left( \theta_f-\theta_0 \right)t - 6\left( \theta_f-\theta_0 \right)t^2.
	\label{Eq:Int16}
\end{equation}
\begin{equation}
	\ddot \theta_{(t)}=6\left( \theta_f-\theta_0 \right) - 12\left( \theta_f-\theta_0 \right)t.
	\label{Eq:Int16}
\end{equation}

%---------------------------------------------------------------------
% Considerations in the trajectory for mass-production
%---------------------------------------------------------------------
\section{Considerations in the trajectory to consider for mass-production}
\label{Ch03:ImproveTrajectories}

The paper dynamics are taken in account to enhance the robot trajectory, and feedback-error learning based control is used to improve the accuracy of the actuators and increase the performance of the robot for mass-production processes.

%---------------------------------------------------------------------
% Spring-back effect
%---------------------------------------------------------------------
\subsection{Spring-back}
\label{Ch03:Spring}
Paper-like materials are composed mainly of cellulose fibers, and considered to be orthotropic materials, this means that their mechanical properties are defined depending on the orientation of the sheet \cite{Schill2015}. Due to the manufacturing process of orthotropic materials, their mechanical properties are defined in machine (MD), cross (CD), and thickness (ZD) directions, see Fig.\ref{Fig:Orthotropic}. The calculations in this paper are done considering only the CD properties, in order to consider the sheet of paper as an isotropic material. However, the proper selection of the direction is still an on-going research for the moment.

Spring-back can be described as a deviation of the sheet after the bending force is unloaded. All work piece materials have a finite modulus of elasticity, so each will undergo a certain elastic recovery upon unloading. This angle of recovery is known as spring-back, and can be used to calculate deformations in flexible materials such as paper. The spring-back effect is characterized using the spring-back factor as \cite{Nebosky2011}:
 
  \begin{equation}
	K_s=\frac{\theta_f}{\theta_i}.
	\label{Eq:SB01}
\end{equation}
where $\theta_i$ denotes the bend angle prior to the removal of forces from the sheet, and  $\theta_f$ is the bend angle after elastic recovery. A
spring-back factor of unity means that the deformed sheet is perfectly plastic, where no elastic recovery has occurred. On the other hand, $K_s= 0$ corresponds to complete elastic recovery.

 \begin{figure}
  \centering
  \begin{minipage}{.45\textwidth}
  \centering
      \includegraphics[width=1.0\linewidth]{Chap03_Orthotropic.png} 
    \caption{Paper fabrication orthotropic directions.}
     \label{Fig:Orthotropic}
    \end{minipage}
      \qquad
    \begin{minipage}{.45\textwidth}
    \centering
     \includegraphics[width=1.0\linewidth]{Chap03_Springback.png} 
    \caption{Spring-back effect in pure bending.}
     \label{Fig:SpringBack}

    \end{minipage}
\end{figure} 

In a pure bending scenario (see Fig. \ref{Fig:SpringBack}), where the the neutral axis is located through the middle of the sheet, the spring-back is calculated as:

 \begin{equation}
	\frac{R_i}{R_f}=4\left( \frac{R_i\sigma_y}{ET}\right) ^3-3\left( \frac{R_i\sigma_y}{ET}\right) + 1.
	\label{Eq:SB02}
\end{equation}
where $Ri$ and  $Rf$ are the initial and final bend radii, $T$ is the sheet thickness, $E$ is the Young's modulus, and $\sigma_y$ is the yield strength. Equation(\ref{Eq:SB01}) can be related to $k_s$, using the bend allowance, which is the arc length of the bend along the neutral axis, and it is calculated as:

 \begin{equation}
	Bend~allowance=\left( R_i+\frac{T}{2}\right) \theta_i =\left(R_f + \frac{T}{2}\right) \theta_f.
	\label{Eq:SB03}
\end{equation}
arranging Eq.(\ref{Eq:SB03}):

 \begin{equation}
	k_s=\frac{\theta_f}{\theta_i}=\frac{ 2\left( R_i/T\right) +1}{ 2\left(R_f/T\right) +1}.
	\label{Eq:SB04}
\end{equation}

Using $R_i$(in this paper is the radii of the edge's curvature of the folding base) and  Eq.(\ref{Eq:SB01}), $R_f$ can be obtained. Then, using $\theta_i$ (in this paper is equal to $3\pi/4$) and Eq.(\ref{Eq:SB04}), the value of $\theta_f$ can be estimated. The value of $\theta_f$ represent the spring-back recovery angle, that can serve us to calculate the recoiled distance $D_r$ of the pre-folded crease pattern after the folding stage as follows:

 \begin{figure}
  \centering
   \includegraphics[width=0.9\linewidth]{Chap03_Springback_Distance.png} 
    \caption{Effects of the spring-back in the crease pattern}
     \label{Distance}
\end{figure} 

 \begin{equation}
	D_r=\sqrt{(w^R_{(n-1)})^2 + (w^R_{(n)})^2 -2w^R_{(n-1)}w^R_{(n)}\cos{(\pi-\theta_f)}}.
	\label{Eq:SB04}
\end{equation}
where $w^R_{(n-1)}$ and $w^R_{(n)}$ are the real width distances between folds of the previous and actual crease lines respectively. This real width is acquired from the advancing sensor in the first stage (i.e advancing sensor, Fig. \ref{Fig:Folding-Gluing}:F4). This distance is computed from every step of the second stage and it lets us know the exact position to stop the forward-backward system in Fig.\ref{Fig:FoldingP}, step H.

%---------------------------------------------------------------------
% Stacking effect
%---------------------------------------------------------------------
\subsection{Stacking effect}
\label{Ch03:Stacking}

The staking effect occurs when the paper is folded multiple times, generating a pile with the double thickness at every fold. Due to the particular design of the folding pattern (see chapter \ref{Ch05}), a stacking effect is generated between steps I-K of the folding process (see Fig.\ref{Fig:FoldingP}). This accumulation of thickness in the bottom wall has to be taken into account to modify the trajectories of the robot as in Fig.\ref{Fig:Stacking}. If this accumulation is not take into account, the robot could get stuck in certain moment of the folding process due to the pressure generated between the folding device, the paper and the bottom wall. The modifications to the global trajectory are represented as follows:

 \begin{equation}
	D_{s(n)}= \left \{ \begin{array}{lr} 
	0; & n=0, \\
	D_{s(n-1)}+2T; & n>0. \end{array} \right.
	\label{Eq:SE01}
\end{equation}

\begin{figure}[h]
  \begin{center}
  \begin{tabular}{ccc}

   \includegraphics[width=0.28\linewidth]{Chap03_Stacking_A.png} &
   \includegraphics[width=0.28\linewidth]{Chap03_Stacking_B.png} &
   \includegraphics[width=0.28\linewidth]{Chap03_Stacking_C.png} 

   \end{tabular}
    \caption{Stacking effect in the folding-gluing stage. From left to right: at $n=1$, at $n=3$, and at $n=6$.}
     \label{Fig:Stacking}
\end{center}
\end{figure} 

All the distances obtained in equations (\ref{Eq:SB04}) and (\ref{Eq:SE01}) are converted into angles to be introduced into the calculations of the trajectory.
Having all trajectory considerations (i.e Spring-back distance, and stacking effect distance), the trajectory of the gluing finger from the gluing step can be represented as follows:

 \begin{equation}
	\theta_{(n)}= \left \{ \begin{array}{lr} 
	\frac{D_r}{2}+D_i; & n=0, \\
	 D_r+ D_{s(n)}+D_i; & n>0. \end{array} \right.
	\label{Eq:ST01}
\end{equation}
where $D_i$ represent the width of the glue-distribution system.
%---------------------------------------------------------------------

%---------------------------------------------------------------------
% Trajectory control with FEL
%---------------------------------------------------------------------
\section{Trajectory control based on feedback-error learning}
\label{Ch03:FEL}

In chapter \ref{Ch04} a deep explanation of the control scheme selected in this paper will be explained. The control scheme used in this chapter is based on feedback-error learning control (FEL). The advantage of FEL over traditional feedback control lies in the improved trajectory following, and impulse response. The use of FEL increases the performance of the system, which reduces the responsive time without losing precision. Many improvements have been made to the original FEL, from changing the feedback controller, to the use of alternatives for the feed-forward controller. In this work, a FEL controller is applied into the robot's trajectory control. This FEL controller uses a traditional proportional-integrative-derivative(PID) as feedback controller, and an adaptive feed-forward controller based on holographic neural networks (HNN) created by \cite{Sutherland1990}.

In cases when a NN is used as Feed-forward controller, is recommended to use NN where a small number of computations have to be done. This means that NNs with a large number of layers or neurons have to be avoided. The HNN is a very special NN which, due to its properties, require an small amount of data to converge, performing few computations, therefore it makes appropriate for FEL.

%---------------------------------------------------------------------


%---------------------------------------------------------------------
% Creating a 3D shape with the proposed robot
%---------------------------------------------------------------------
\section{Creating a 3D shape with the proposed robot}
\label{Ch03:Results}

The experiments carried out in this chapter have the following steps: First, the pattern is created using the SOR methodology by specifying a target profile and rotate it by a number of $N$ segments (see chapter \ref{Ch05}). The results of the applied SOR methodology are the segment width and the stamp shape to be allocated in the gluing stamp location(Fig.\ref{Fig:Folding-Gluing}:G3). This information serve us to generate the trajectories of the folding stage of the robot(Fig.\ref{Fig:Folding-Gluing}:F). After the paper is pre-folded in the folding stage enters to the folding-gluing stage (Fig.\ref{Fig:Folding-Gluing}:G). To create the trajectories of this stage, information from the folding step is used. Using the information of the output widths from the folding stage, the spring-back effect can be estimated and used to generate the trajectory of the folding-gluing stage. This value is calculated using the mechanical properties of the paper (i.e. paper type, and thickness), and the real values of the widths are obtained from the advancing sensor(Fig.\ref{Fig:Folding-Gluing}:F4) in the folding stage. Both trajectories are loaded into the corresponding NXT Brain-Bricks (Fig.\ref{Fig:Folding-Gluing}:F5 and G7), the information from the first brick is transmitted into the second brick through Bluetooth communication. Each one of the parameters(i.e. FEL with HNN, spring-back effect, and stacking effect) are individually measured and compared with the expected result to observe their effects in the final 3D shape. The error varies depending on the gluing area. If the gluing area is wide, the error due to variations in the line alignment increases. All these experiments are carried out with white Bond paper sheets, and using a 10-side box, similar to the one in Fig.\ref{Fig:Results}.(b). For the experiments with HNN, the holographic memory is initially set with matrices of zeros  ($H_{(t=0)}=0$ and $G_{(t=0)}=0$). The parameters of the crease pattern and paper for these experiments are exposed in Table\ref{Tab:Parameters}:

\begin{table}
\caption{Experiment parameters}
\label{Tab:Parameters}
\begin{center}
\begin{tabular}{l|c|c|c}

\bf Parameter & \bf Symbol & \bf Value & \bf Unit \\
\hline
Thickness & T & $0.98\times10^{-3}$ & m \\
Young's Modulus of paper CD & E & $4.94\times10^9$ & Pa \\
Yield strength of paper CD & $\sigma_y$ & $5.03\times10^7$ & Pa \\
Width & W & $26\times10^{-3}$ & m \\
Number of segments & N & 10 & $-$ 

\end{tabular}
\end{center}
\end{table}

%---------------------------------------------------------------------
% Effects of the paper in the dynamics
%---------------------------------------------------------------------
\subsection{Effects of the paper dynamics in the robot trajectory}
\label{Sub:Effects}

Here, the effects of the dynamics of the paper in the robot trajectory calculations are analyzed. For this experiments, the effects of each one of the parameters that affect the trajectory calculations, i.e. FEL with and without HNN, the spring-back and stacking effects, are analyzed. Using equations (\ref{Eq:Q01}) and  (\ref{Eq:Q02}), the error reduction from each one of these parameter can be estimated by comparing the error between the expected area and the resulting area using equations (\ref{Eq:Q01}) and (\ref{Eq:Q02}). Two PIDs are used as feedback controllers, one for the first stage and other for the second stage, with: $P_1=55,I_1=75,D_1=0.1$, and $P_2=40,I_2=20, and~D_2=0$. The sample time is set to $0.05s$. Initially, the error of the proposed machine using only the PID controller is calculated. Then, one by one, each parameter is added, to estimate the error reduction. The results of these experiments can be observed in Table.\ref{Tab:PaperE}.

\begin{table}
      \caption{Effects of the paper in the final shape}
       \label{Tab:PaperE}
       \tiny
      \centering
        \begin{tabular}{p{0.20\textwidth}|p{0.11\textwidth} |p{0.11\textwidth}|p{0.11\textwidth}|p{0.11\textwidth}|p{0.11\textwidth}}
            \bf Trajectory & \bf E1($m$) & \bf E2($m$)& \bf Expected Area($m^2$)& \bf Real  Area($m^2$)& \bf Error  $\%$ \\
            \hline
            PID & $2.9096\times10^{-7}$ & $1.8970\times10^{-5}$	& $0.0260$ & 0.025938 & 1.778\\
            PID$+$FEL & $6.4258\times10^{-8}$ & $1.7414\times10^{-5}$	 &  $0.0260$ & 0.025939 &  1.614\\
            PID$+$FEL$+$Spring back & $6.4258\times10^{-8}$ & $1.2979\times10^{-5}$ &  $0.0260$ & 0.025954 & 1.204\\
            PID$+$FEL$+$Spring back$+$Stacking & $6.4258\times10^{-8}$ & $1.2657\times10^{-5}$ &  $0.0260$ & 0.025955 &1.174
        \end{tabular}
\end{table}

The error percentage($E\%$) is calculated as: $E\%=\left|100\frac{Real~Area}{Expected~Area}-100\right|$.
Although the first three experiments were carried out without the stacking effect calculation, it is recommended to do it using this parameter to prevent unexpected stops in the robot's movement. We can observe from tables \ref{Tab:PaperE} and \ref{Tab:Speed} that all the parameters reduce the error in the trajectories, been the spring-back effect calculation the parameter that reduces the error further.

%---------------------------------------------------------------------
% Effects of increasing the speed
%---------------------------------------------------------------------
\subsection{Effects of increasing the speed of the shape's creation}
\label{Sub:Speed}
Other objective in this dissertation is to use the proposed robot for mass-production procedures. Due to the increases in a shape's creation speed, the error increases too. Here, the use of FEL control is proposed to counter the increases in error due to the speed changes. Table.\ref{Tab:Speed} shows the results of increasing the speed in the shape's creation process, using the same shape and paper's type from the past experiment (see table. \ref{Tab:Parameters}).

\begin{table}
      \caption{Effects of increasing the shape's creation speed ($E\%$)}
       \label{Tab:Speed}
      \centering
        \begin{tabular}{p{0.22\textwidth}|c|c|c|c|c|c}
        ~& \multicolumn{6}{c}{\bf Speed multiplier}\\
       		\hline
            \bf Controller & \bf 1.0X & \bf 1.2X& \bf 1.4X & \bf 1.6X & \bf 1.8X &  \bf 2.0X \\
            \hline
            PID+Spring back+Stacking & 1.192 & 1.355 & 1.908 & 2.369 & 2.3397 & 2.3208\\
            PID+FEL+Spring back+Stacking &01.174 & 1.185 &  1.261 & 1.405 & 2.1796 & 2.2011
        \end{tabular}
\end{table}
The error requirements vary depending on each application and 3D shape. However, as can be observed in table.\ref{Tab:Speed}, the use of the HNN as FEL controller increases the performance of the machine, allowing to create more shapes in less time.

%---------------------------------------------------------------------
%Results
%---------------------------------------------------------------------
\subsection{Resulting shapes built by the proposed robot}
\label{Sub:Results}
To demonstrate the applicability of the proposed robot, three different shapes, a pyramid, a hexagonal box, and a dome, are automatically folded. Their corresponding crease pattern, expected 3D shape, and final products will be shown in fig.\ref{Fig:Results}.  

\begin{figure}[h]
  \begin{center}
  \begin{tabular}{m{.01\linewidth} m{.18\linewidth} m{.18\linewidth} m{.18\linewidth} m{.18\linewidth}}
  ~ & \multicolumn{1}{c}{Profile} & \multicolumn{1}{c}{Crease pattern} & \multicolumn{1}{c}{3D approximation} & \multicolumn{1}{c}{Shape after open} \\
   \bf(a) & 
   \includegraphics[width=1.0\linewidth]{Chap03_Result_01_Profile.png} &
   \includegraphics[width=1.0\linewidth]{Chap03_Result_01_Pattern.png} &
   \includegraphics[width=1.0\linewidth]{Chap03_Result_01_3D.png} &
   \includegraphics[width=1.0\linewidth]{Chap03_Result_01_Real.png} \\

   \bf(b) & 
   \includegraphics[width=1.0\linewidth]{Chap03_Result_02_Profile.png} &
   \includegraphics[width=1.0\linewidth]{Chap03_Result_02_Pattern.png} &
   \includegraphics[width=1.0\linewidth]{Chap03_Result_02_3D.png} &
   \includegraphics[width=1.0\linewidth]{Chap03_Result_02_Real.png} \\
    
    \bf(c) &
    \includegraphics[width=1.0\linewidth]{Chap03_Result_03_Profile.png} &
    \includegraphics[width=1.0\linewidth]{Chap03_Result_03_Pattern.png} &
    \includegraphics[width=1.0\linewidth]{Chap03_Result_03_3D.png} &
    \includegraphics[width=1.0\linewidth]{Chap03_Result_03_Real.png} 
      \end{tabular}
    \caption{Result profiles, patterns and 3D shapes. (a) Pyramid ($N=4, W=14.1mm$), (b) Hexagonal box ($N=6, W=11.14mm$), (c) Dome($N=10, W=7.0mm$).}
     \label{Fig:Results}
\end{center}
\end{figure} 
 
 The complexity reduction is possible due to adding gluing areas into the proposed crease pattern, that reduces movements in the robot by executing multiple tasks at the same time. A manipulation is considered as a combination of movements required to execute a task, e.g. a fold, a turn over, a rotation, or a squash \cite{Tanaka2007}. Table.\ref{Table:NOM} exposes the number of manipulations in a group of crease patterns folded by hand and the proposed robot.
 
\begin{table}
\begin{center}

	\caption{Number of manipulations.}
	\label{Table:NOM} 	
	\begin{tabular}{ p{0.30\textwidth} | p{0.18\textwidth}  | p{0.18\textwidth} | p{0.18\textwidth}   }
		Pattern Name & Human's  $\#$ manipulations & Robot's  $\#$ manipulations & $\#$ of manipulations reduced \\
  		\hline			
  		Pyramid (Fig.\ref{Fig:Results}(a) & 40 & 16 & 24 \\
  		Box (Fig.\ref{Fig:Results}(b) & 36 & 18 & 18 \\
  		Dome (Fig.\ref{Fig:Results}(c) & 100 & 20 & 80 
	\end{tabular}
\end{center}
\end{table}

%---------------------------------------------------------------------
% Summary
%---------------------------------------------------------------------
\section{Summary}
\label{SummaryRobot}

In this chapter, a folding-gluing robot able to build a 3D shape of a SOR-based crease pattern has been developed. Here, we show that using simple folds in combination with gluing areas, interesting 3D shapes can be achieved with the proposed robot (see Fig.\ref{Fig:Results}). The crease pattern used by this robot is based on a SOR methodology created in \cite{Mitani2009}. Several parameters of the crease patterns and paper type are used to calculate a series of trajectory enhancers, i.e. a FEL controller with HNN, and a spring-back and stacking effects calculations.  The results show that each one of these enhancers reduces the ME, produced by displacements or slips in the paper sheet. In order to analyze the performance of the robot for mass-production purposes, a FEL controller using a HNN is proposed. As can be observed in table.\ref{Tab:Speed}, the HNN keeps the displacements controlled, even in twice the initial speed (2.0X). This permits us to reduce the production time because more shapes can be created in less time preserving the quality within a frame.